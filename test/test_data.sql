--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.2
-- Dumped by pg_dump version 9.6.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: tasks; Type: SCHEMA; Schema: -; Owner: -
--

-- CREATE SCHEMA tasks;


-- SET search_path = tasks, pg_catalog;

SET default_with_oids = false;

--
-- Name: ports; Type: TABLE; Schema: tasks; Owner: -
--

CREATE TABLE ports (
    code text NOT NULL,
    name text NOT NULL,
    parent_slug text NOT NULL
);


--
-- Name: prices; Type: TABLE; Schema: tasks; Owner: -
--

CREATE TABLE prices (
    orig_code text NOT NULL,
    dest_code text NOT NULL,
    day date NOT NULL,
    price integer NOT NULL
);


--
-- Name: regions; Type: TABLE; Schema: tasks; Owner: -
--

CREATE TABLE regions (
    slug text NOT NULL,
    name text NOT NULL,
    parent_slug text
);

--
-- Data for Name: regions; Type: TABLE DATA; Schema: tasks; Owner: -
--
-- One region with 2 subregions, one standalone region

INSERT INTO regions VALUES ('region_a_1', 'Ra1', 'region_a');
INSERT INTO regions VALUES ('region_a_2', 'Ra2', 'region_a');
INSERT INTO regions VALUES ('region_a', 'Ra');
INSERT INTO regions VALUES ('region_b', 'Rb');

--
-- Data for Name: ports; Type: TABLE DATA; Schema: tasks; Owner: -
--
-- Some regions belong to subregions, some to higher level regions

INSERT INTO ports VALUES ('PORTA1a', 'A1a', 'region_a_1');
INSERT INTO ports VALUES ('PORTA1b', 'A1b', 'region_a_1');
INSERT INTO ports VALUES ('PORTA2', 'A2', 'region_a_2');
INSERT INTO ports VALUES ('PORTAa', 'Aa', 'region_a');
INSERT INTO ports VALUES ('PORTAb', 'Ab', 'region_a');
INSERT INTO ports VALUES ('PORTB', 'B', 'region_b');

--
-- Data for Name: prices; Type: TABLE DATA; Schema: tasks; Owner: -
--
-- region_a to region_a has 4 prices, but PORTA1a to PORTA1b only has 2 for 2016-01-01

INSERT INTO prices VALUES ('PORTA1a', 'PORTA1b', '2016-01-01', 1);
INSERT INTO prices VALUES ('PORTA1a', 'PORTA1b', '2016-01-01', 3);
INSERT INTO prices VALUES ('PORTA1a', 'PORTAa', '2016-01-01', 5);
INSERT INTO prices VALUES ('PORTAa', 'PORTA1a', '2016-01-01', 3);

INSERT INTO prices VALUES ('PORTAb', 'PORTB', '2016-01-02', 5);

--
-- Name: ports ports_pkey; Type: CONSTRAINT; Schema: tasks; Owner: -
--

ALTER TABLE ONLY ports
    ADD CONSTRAINT ports_pkey PRIMARY KEY (code);


--
-- Name: regions regions_pkey; Type: CONSTRAINT; Schema: tasks; Owner: -
--

ALTER TABLE ONLY regions
    ADD CONSTRAINT regions_pkey PRIMARY KEY (slug);


--
-- Name: ports ports_parent_slug_fkey; Type: FK CONSTRAINT; Schema: tasks; Owner: -
--

ALTER TABLE ONLY ports
    ADD CONSTRAINT ports_parent_slug_fkey FOREIGN KEY (parent_slug) REFERENCES regions(slug);


--
-- Name: prices prices_dest_code_fkey; Type: FK CONSTRAINT; Schema: tasks; Owner: -
--

ALTER TABLE ONLY prices
    ADD CONSTRAINT prices_dest_code_fkey FOREIGN KEY (dest_code) REFERENCES ports(code);


--
-- Name: prices prices_orig_code_fkey; Type: FK CONSTRAINT; Schema: tasks; Owner: -
--

ALTER TABLE ONLY prices
    ADD CONSTRAINT prices_orig_code_fkey FOREIGN KEY (orig_code) REFERENCES ports(code);


--
-- Name: regions regions_parent_slug_fkey; Type: FK CONSTRAINT; Schema: tasks; Owner: -
--

ALTER TABLE ONLY regions
    ADD CONSTRAINT regions_parent_slug_fkey FOREIGN KEY (parent_slug) REFERENCES regions(slug);


--
-- PostgreSQL database dump complete
--

