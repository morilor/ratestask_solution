import itertools
from unittest import TestCase

from flask import Flask
from psycopg2 import connect

from solution_app.models.model import ModelErrorDB, ModelErrorInput, ModelSQL
from solution_app.services.backend import Backend
from solution_app.services.settings import SETTINGS


class BackendTestCase(TestCase):
    # We only test Backend+ModelSQL together, because it covers most of the code and fits the current scope
    # of the project. A larger project would justify a separation of Backend and Model tests.
    # Since we need an actual db for tests, we need to spin up a container using the following command:
    # docker run --rm -p 5432:5432 --name postgres_test -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=pgfortests postgres

    @staticmethod
    def _clean_db():
        with connect(database=SETTINGS["POSTGRES_DB"],
                     host=SETTINGS["POSTGRES_HOST"],
                     user=SETTINGS["POSTGRES_USER"],
                     password=SETTINGS["POSTGRES_PASSWORD"],
                     port=SETTINGS["POSTGRES_PORT"]) as conn:
            cur = conn.cursor()
            cur.execute("DROP TABLE IF EXISTS prices")
            cur.execute("DROP TABLE IF EXISTS ports")
            cur.execute("DROP TABLE IF EXISTS regions")

    def setUp(self) -> None:
        self._clean_db()
        self.backend = Backend(ModelSQL())
        self.app = Flask('test')
        with connect(database=SETTINGS["POSTGRES_DB"],
                     host=SETTINGS["POSTGRES_HOST"],
                     user=SETTINGS["POSTGRES_USER"],
                     password=SETTINGS["POSTGRES_PASSWORD"],
                     port=SETTINGS["POSTGRES_PORT"]) as conn:
            cur = conn.cursor()
            cur.execute(open("test/test_data.sql", "r").read())

    def tearDown(self) -> None:
        self._clean_db()

    def test_index(self):
        with self.app.app_context():
            resp, status = self.backend.status()
        self.assertEqual(status, 200)
        resp_data = resp.json
        expected_data = {
            'status': {
                'App': 'OK',
                'Database': 'OK'
            }
        }
        self.assertDictEqual(resp_data, expected_data)

    def test_subregions_unknown(self):
        with self.app.test_client() as c:
            c.get('regions?origin=UNKNOWN')
            with self.app.app_context():
                resp, status = self.backend.subregions()
        self.assertEqual(status, 200)
        resp_data = resp.json
        self.assertListEqual(resp_data, [])

    def test_subregions_region(self):
        with self.app.test_client() as c:
            c.get('regions?origin=region_a')
            with self.app.app_context():
                resp, status = self.backend.subregions()
        self.assertEqual(status, 200)
        resp_data = resp.json
        self.assertListEqual(list(sorted(resp_data)), list(sorted(['region_a_1', 'region_a_2', 'region_a'])))

    def test_subregions_empty(self):
        with self.app.test_client() as c:
            c.get('region')
            with self.app.app_context():
                resp, status = self.backend.subregions()
        self.assertEqual(status, 200)
        resp_data = resp.json
        self.assertListEqual(list(sorted(resp_data)), list(sorted(['region_a_1', 'region_a',
                                                                   'region_a_2', 'region_b'])))

    def test_ports_port(self):
        with self.app.test_client() as c:
            c.get('ports?origin=PORTA1a')
            with self.app.app_context():
                resp, status = self.backend.ports()
        self.assertEqual(status, 200)
        resp_data = resp.json
        self.assertListEqual(resp_data, ['PORTA1a'])

    def test_ports_port_unknown(self):
        with self.app.test_client() as c:
            c.get('ports?origin=UNKNOWN')
            with self.app.app_context():
                resp, status = self.backend.ports()
        self.assertEqual(status, 200)
        resp_data = resp.json
        self.assertListEqual(resp_data, [])

    def test_ports_region(self):
        with self.app.test_client() as c:
            c.get('ports?origin=region_a')
            with self.app.app_context():
                resp, status = self.backend.ports()
        self.assertEqual(status, 200)
        resp_data = resp.json
        self.assertListEqual(list(sorted(resp_data)), list(sorted(['PORTA1a', 'PORTA1b', 'PORTA2',
                                                                   'PORTAa', 'PORTAb'])))

    def test_ports_empty(self):
        with self.app.test_client() as c:
            c.get('ports')
            with self.app.app_context():
                resp, status = self.backend.ports()
        self.assertEqual(status, 200)
        resp_data = resp.json
        self.assertListEqual(list(sorted(resp_data)), list(sorted(['PORTA1a', 'PORTA1b', 'PORTAa',
                                                                   'PORTAb', 'PORTA2', 'PORTB'])))

    def test_prices_port(self):
        with self.app.test_client() as c:
            c.get('prices?origin=PORTA1a&destination=PORTA1b')
            with self.app.app_context():
                resp, status = self.backend.prices()
        self.assertEqual(status, 200)
        resp_data = resp.json
        expected_data = [
            {'day': '2016-01-01',
             'origin': 'PORTA1a',
             'destination': 'PORTA1b',
             'price': 1},
            {'day': '2016-01-01',
             'origin': 'PORTA1a',
             'destination': 'PORTA1b',
             'price': 3}
        ]
        for dict1, dict2 in itertools.zip_longest(resp_data, expected_data, fillvalue={}):
            self.assertDictEqual(dict1, dict2)

    def test_prices_region(self):
        with self.app.test_client() as c:
            c.get('prices?origin=region_a&destination=region_a')
            with self.app.app_context():
                resp, status = self.backend.prices()
        self.assertEqual(status, 200)
        resp_data = resp.json
        expected_data = [
            {'day': '2016-01-01',
             'origin': 'PORTA1a',
             'destination': 'PORTA1b',
             'price': 1},
            {'day': '2016-01-01',
             'origin': 'PORTA1a',
             'destination': 'PORTA1b',
             'price': 3},
            {'day': '2016-01-01',
             'origin': 'PORTA1a',
             'destination': 'PORTAa',
             'price': 5},
            {'day': '2016-01-01',
             'origin': 'PORTAa',
             'destination': 'PORTA1a',
             'price': 3}
        ]
        for dict1, dict2 in itertools.zip_longest(resp_data, expected_data, fillvalue={}):
            self.assertDictEqual(dict1, dict2)

    def test_prices_empty(self):
        with self.app.test_client() as c:
            c.get('prices')
            with self.app.app_context():
                resp, status = self.backend.prices()
        self.assertEqual(status, 200)
        resp_data = resp.json
        expected_data = [

            {'day': '2016-01-01',
             'origin': 'PORTA1a',
             'destination': 'PORTA1b',
             'price': 1},
            {'day': '2016-01-01',
             'origin': 'PORTA1a',
             'destination': 'PORTA1b',
             'price': 3},
            {'day': '2016-01-01',
             'origin': 'PORTA1a',
             'destination': 'PORTAa',
             'price': 5},
            {'day': '2016-01-01',
             'origin': 'PORTAa',
             'destination': 'PORTA1a',
             'price': 3},
            {'day': '2016-01-02',
             'origin': 'PORTAb',
             'destination': 'PORTB',
             'price': 5}
        ]
        for dict1, dict2 in itertools.zip_longest(resp_data, expected_data, fillvalue={}):
            self.assertDictEqual(dict1, dict2)

    def test_rates_port(self):
        with self.app.test_client() as c:
            c.get('rates?origin=PORTA1a&destination=PORTA1b&date_from=2016-01-01&date_to=2016-01-01')
            with self.app.app_context():
                resp, status = self.backend.rates()
        self.assertEqual(status, 200)
        resp_data = resp.json
        expected_data = [
            {
                'day': '2016-01-01',
                'average_price': None
            }
        ]
        for dict1, dict2 in itertools.zip_longest(resp_data, expected_data, fillvalue={}):
            self.assertDictEqual(dict1, dict2)

    def test_rates_region(self):
        with self.app.test_client() as c:
            c.get('rates?origin=region_a&destination=region_a&date_from=2016-01-01&date_to=2016-01-01')
            with self.app.app_context():
                resp, status = self.backend.rates()
        self.assertEqual(status, 200)
        resp_data = resp.json
        expected_data = [
            {
                'day': '2016-01-01',
                'average_price': 3
            }
        ]
        for dict1, dict2 in itertools.zip_longest(resp_data, expected_data, fillvalue={}):
            self.assertDictEqual(dict1, dict2)

    def test_rates_empty(self):
        with self.app.test_client() as c:
            c.get('rates')
            with self.app.app_context():
                resp, status = self.backend.rates()
        self.assertEqual(status, 200)
        resp_data = resp.json
        expected_data = [
            {
                'day': '2016-01-01',
                'average_price': 3
            },
            {
                'day': '2016-01-02',
                'average_price': None
            }
        ]
        for dict1, dict2 in itertools.zip_longest(resp_data, expected_data, fillvalue={}):
            self.assertDictEqual(dict1, dict2)

    def test_injection_itself(self):
        def rates_query_vulnerable(model, date_from='', date_to='', origin='', destination=''):
            conditions = []
            if date_from:
                conditions.append(f"day >= '{date_from}'")  # vulnerability
            if date_to:
                conditions.append(f"day <= %(date_to)s")

            if not conditions:
                where_clause = ''
            else:
                where_clause = 'WHERE ' + ' AND '.join(conditions)

            q = f"""
            SELECT
                day,
                CASE WHEN COUNT(*)>3 THEN AVG(price) ELSE null END
            FROM ({model.prices_query(origin, destination)}) pr
            {where_clause}
            GROUP BY day
            ORDER BY day
            """
            return q

        # unwrapping a decorator to avoid exception handlers
        self.backend.prices = self.backend.prices.__wrapped__.__get__(self.backend,
                                                                      self.backend.__class__)

        # adding an original vulnerable function back
        self.backend.model.rates_query = rates_query_vulnerable.__get__(self.backend.model,
                                                                        self.backend.model.__class__)
        with self.app.test_client() as c:
            injection_request = "rates?date_from=2016-01-02' AND day<='2016-01-03' GROUP BY day; {injection} " \
                                "SELECT TO_DATE('2016-01-02', 'YYYYMMDD') as day, 7357 WHERE 'a' = 'a "
            c.get(injection_request.format(injection='DROP TABLE prices;'))
            with self.app.app_context():
                _ = self.backend.rates()
        with self.app.test_client() as c:
            c.get('prices')
            with self.app.app_context():
                with self.assertRaises(ModelErrorDB) as assertion:
                    _ = self.backend.prices()
                self.assertTrue(str(assertion.exception).startswith('relation "public.prices" does not exist'))

    def test_rates_injection(self):

        # unwrapping a decorator to avoid exception handlers
        self.backend.rates = self.backend.rates.__wrapped__.__get__(self.backend,
                                                                    self.backend.__class__)

        with self.app.test_client() as c:
            injection_request = "rates?date_from=2016-01-02' AND day<='2016-01-03' GROUP BY day; {injection} " \
                                "SELECT TO_DATE('2016-01-02', 'YYYYMMDD') as day, 7357 WHERE 'a' = 'a "
            c.get(injection_request.format(injection='DROP TABLE prices;'))
            with self.app.app_context():
                with self.assertRaises(ModelErrorInput) as assertion:
                    _ = self.backend.rates()
                self.assertTrue(str(assertion.exception).startswith('invalid input syntax for type date'))

    def test_rates_error_400(self):
        with self.app.test_client() as c:
            c.get('rates?origin=region_a&destination=region_a&date_from=invalid_date&date_to=2016-01-01')
            with self.app.app_context():
                resp, status = self.backend.rates()
        self.assertEqual(status, 400)
        resp_data = resp.json
        expected_data = 'Invalid query parameters.'
        self.assertEqual(resp_data, expected_data)

    def test_rates_error_404(self):
        original_pass = SETTINGS['POSTGRES_PASSWORD']
        try:
            SETTINGS['POSTGRES_PASSWORD'] = 'wrong_password'
            with self.app.test_client() as c:
                c.get('rates?origin=region_a&destination=region_a&date_from=2016-01-01&date_to=2016-01-01')
                with self.app.app_context():
                    resp, status = self.backend.rates()
            self.assertEqual(status, 404)
            resp_data = resp.json
            expected_data = 'Data could not be accessed.'
            self.assertEqual(resp_data, expected_data)
        finally:
            SETTINGS['POSTGRES_PASSWORD'] = original_pass

    def test_rates_error_500(self):
        with self.app.test_client() as c:
            c.get('rates?origin=region_a&destination=region_a&date_from=invalid_date&date_to=2016-01-01')
            with self.app.app_context():
                def error_func():
                    raise Exception

                self.backend.model.get_rates = error_func
                resp, status = self.backend.rates()
        self.assertEqual(status, 500)
        resp_data = resp.json
        expected_data = 'Internal server error.'
        self.assertEqual(resp_data, expected_data)
