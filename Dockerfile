FROM python:3.11
COPY solution_app ./solution_app
COPY app.py .
COPY config.env .
COPY requirements.txt .
RUN pip install -r requirements.txt
ENTRYPOINT gunicorn 'app:app' -b '0.0.0.0:5000'