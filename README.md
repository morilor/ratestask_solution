# Rates Task Solution

## Introduction

The application provides a solution for the [ratestask](https://github.com/xeneta/ratestask).

### Development process

It took me about 3 hours to develop a simple flask app with a 50-line 
SQL query inside, that solved the whole task within one query. The rest of the
development was to ensure the project readability and maintainability.
For that purpose, the following features were added:
- **Unittests** to ensure code quality
- **Logging** to improve maintainability
- **Split the query into blocks** to improve readability and to provide additional 
APIs from resulting sub-queries.
- **GitLab CI** to run unittests automatically, so no untested branch would be merged into main
- **Docker** to simplify deployment and ensure consistency on different machines
- **Caching** to make frequent requests quicker
- **Swagger** to provide a manual for the app within the API itself
- **SQL Injection protection** to prevent harmful SQL commands from being executed
- **Error codes and exceptions** to provide clear responses in cases of failed queries,
and to prevent the inner exceptions from ever showing on the public side of the API.
- **Modular structure** to separate the main part of the API from swagger, 
and to provide a structure for additional module support
- **Generalised backend and model api and type hints** to develop, update and substitute the services 
independently of the others. For example, we might need to switch from pure SQL to ORM data model.
- **App factory** to provide a cleaner app building pattern 

## API

### `/`
Provides the app status, both for the app itself and for the data connection.

### `/swagger`
An interactive [Swagger UI](https://swagger.io/tools/swagger-ui/) API resource description.

### `/rates`
Gets all average prices for each day between origin and destination,
if there are more than 3 shipments that day, otherwise returns null.

### parameters:
Each parameter can be omitted
- `date_from=YYYY-MM-DD` - if specified, restricts the start date in the results
- `date_to=YYYY-MM-DD` - if specified, restricts the end date in the results
- `origin=port_or_region` - if specified, only checks the shipments 
with this port or the ports from this region as an origin
- `destination=port_or_region` - if specified, only checks the shipments
with this port or the ports from this region as a destination


### `/regions`
Gets all regions filtered by the parent region. 
A region is considered belonging to its own group.

### parameters:
Each parameter can be omitted
- `origin=port_or_region` - if specified, only provides regions that 
belong to the given region


### `/ports`
Gets all ports filtered by the region.

### parameters:
Each parameter can be omitted
- `origin=port_or_region` - if specified, only provides ports that 
belong to the given region; if port code is given, returns that code


### `/prices`
Gets all prices for each shipment between origin and destination.

### parameters:
Each parameter can be omitted
- `origin=port_or_region` - f specified, only checks the shipments 
with this port or the ports from this region as an origin
- `destination=port_or_region` - if specified, only checks the shipments
with this port or the ports from this region as a destination

### Response codes
- **200** - the operation was successful
- **400** - the query parameters are invalid
- **404** - the data could not be accessed
- **500** - internal server error


## Installation and launching

### Configuration
You either need to change the contents of `config.env` or to set the corresponding environment variables.
Most importantly, _POSTGRES\_*_ variables need to be set to provide ratestask database credentials.

### Docker

To build a docker image:
```commandline
docker build -t ratestask_solution .
```

To launch the image:
```commandline
docker run --rm -it -p 0.0.0.0:5000:5000 --env-file config.env --name ratestask_solution ratestask_solution
```
If the ratestask database is also in an image, you need to check that
the images belong to the same docker network:
```commandline
docker network create -d bridge my-net
docker run --rm -it --network my-net -p 0.0.0.0:5432:5432 --name ratestask ratestask
```
Set the environment variable or the config parameter as `POSTGRES_HOST=ratestask`
```commandline
docker run --rm -it --network my-net -p 0.0.0.0:5000:5000 --env-file config.env --name ratestask_solution ratestask_solution
```

### From command line
To launch the app locally:
```commandline
pip install -r requirements.txt
flask run
```