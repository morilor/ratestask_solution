import logging
from abc import ABC, abstractmethod
from functools import lru_cache
from typing import Dict, List, Tuple, Any

from psycopg2.errors import DataError
from psycopg2 import connect, DatabaseError

from solution_app.services.settings import SETTINGS

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(SETTINGS['LOGGING_LEVEL'])


class ModelError(Exception):  # we cannot use psycopg2 errors, since models are interchangeable
    pass


class ModelErrorDB(ModelError):
    pass


class ModelErrorInput(ModelError):
    pass


class ModelAPI(ABC):
    """
    The data layer of the project, handles all db interactions, returns raw data, never touches requests directly.
    """
    @abstractmethod
    def get_status(self, *args, **kwargs) -> Dict:
        pass

    @abstractmethod
    def get_subregions(self, *args, **kwargs) -> List[Tuple[Any]]:
        pass

    @abstractmethod
    def get_ports(self, *args, **kwargs) -> List[Tuple[Any]]:
        pass

    @abstractmethod
    def get_prices(self, *args, **kwargs) -> List[Tuple[Any]]:
        pass

    @abstractmethod
    def get_rates(self, *args, **kwargs) -> List[Tuple[Any]]:
        pass


class ModelSQL(ModelAPI):
    @property
    def conn(self):
        return connect(database=SETTINGS["POSTGRES_DB"],
                       host=SETTINGS["POSTGRES_HOST"],
                       user=SETTINGS["POSTGRES_USER"],
                       password=SETTINGS["POSTGRES_PASSWORD"],
                       port=SETTINGS["POSTGRES_PORT"])

    @lru_cache(maxsize=500)
    # We added parameters explicitly instead of forwarding kwargs to execute() for the purposes of caching.
    def query(self, q: str, date_from: str = '', date_to: str = '', 
              origin: str = '', destination: str = '') -> List[Tuple[Any]]:
        # logging a clean query without extra blank spaces
        logger.debug("Query: \n " + '\n'.join([s for s in q.splitlines() if s.replace(' ', '')]))

        try:
            with self.conn as conn:
                cursor = conn.cursor()
                cursor.execute(q, {'date_from': date_from, 'date_to': date_to,
                                   'origin': origin, 'destination': destination})
        except DataError as e:
            raise ModelErrorInput(e)
        except DatabaseError as e:
            raise ModelErrorDB(e)
        result = cursor.fetchall()

        return result

    @staticmethod
    def cte_query(origin=''):
        q = f"""
        WITH RECURSIVE get_subregions (slug) as (
            SELECT slug, parent_slug FROM public.regions
            {f"WHERE parent_slug=%(origin)s" if origin else ''}
            UNION
            (
                SELECT r.slug, r.parent_slug
                FROM public.regions r
                INNER JOIN 
                get_subregions sr ON  sr.slug = r.parent_slug
            )
        )
        """
        return q

    def subregions_query(self, origin=''):
        return f"""
        {self.cte_query(origin)}
        SELECT slug as origin FROM get_subregions
        {'''
        UNION ALL    -- adding the %(origin)s itself if it is a valid region
        (
            SELECT origin
            FROM (
                SELECT CASE WHEN (%(origin)s in (SELECT DISTINCT slug from public.regions))
                    THEN %(origin)s ELSE NULL END as origin
            ) self
            WHERE NOT origin IS NULL
        )
        ''' if origin else ''}
        """

    def ports_query(self, origin=''):
        return f"""
            SELECT p.code
            FROM
            ({self.subregions_query(origin)}) as r
            INNER JOIN public.ports p ON p.parent_slug = r.origin OR p.code = r.origin
            {'''
            UNION ALL    -- adding the %(origin)s itself if it is a valid port
            (
                SELECT origin
                FROM (
                    SELECT CASE WHEN (%(origin)s in (SELECT DISTINCT code from public.ports))
                        THEN %(origin)s ELSE NULL END as origin
                ) self
                WHERE NOT origin IS NULL
            )
            ''' if origin else ''}
        """

    def prices_query(self, origin='', destination=''):
        # we replace variable names to distinguish between 2 ports sub-queries
        q = f"""
        SELECT * FROM public.prices
        WHERE orig_code IN
        ({self.ports_query(origin)})
        AND dest_code IN
        ({self.ports_query(destination).replace('%(origin)s', '%(destination)s')})
        """
        return q

    def rates_query(self, date_from='', date_to='', origin='', destination=''):
        conditions = []
        if date_from:
            conditions.append(f"day >= %(date_from)s")
        if date_to:
            conditions.append(f"day <= %(date_to)s")

        if not conditions:
            where_clause = ''
        else:
            where_clause = 'WHERE ' + ' AND '.join(conditions)

        q = f"""
        SELECT
            day,
            CASE WHEN COUNT(*)>3 THEN AVG(price) ELSE null END
        FROM ({self.prices_query(origin, destination)}) pr
        {where_clause}
        GROUP BY day
        ORDER BY day
        """
        return q

    def get_status(self) -> Dict:
        result = {'status': {'App': 'OK'}}
        try:
            self.query("SELECT 1")
            result['status']['Database'] = 'OK'

        except ModelError:
            result['status']['Database'] = 'FAILED'

        return result

    def get_subregions(self, request_args) -> List[Tuple[Any]]:
        q = self.subregions_query(**request_args)
        return self.query(q, **request_args)

    def get_ports(self, request_args) -> List[Tuple[Any]]:
        q = self.ports_query(**request_args)
        return self.query(q, **request_args)

    def get_prices(self, request_args) -> List[Tuple[Any]]:
        q = self.prices_query(**request_args)
        return self.query(q, **request_args)

    def get_rates(self, request_args) -> List[Tuple[Any]]:
        q = self.rates_query(**request_args)
        return self.query(q, **request_args)
