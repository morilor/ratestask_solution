from flask import Blueprint

from solution_app.services.backend import BackendAPI


class SolutionBlueprint(Blueprint):
    def __init__(self, backend: BackendAPI):
        super().__init__('solution', __name__)
        self.add_url_rule('/', 'status', backend.status, methods=['GET'])
        self.add_url_rule('/regions', 'region', backend.subregions, methods=['GET'])
        self.add_url_rule('/ports', 'ports', backend.ports, methods=['GET'])
        self.add_url_rule('/prices', 'prices', backend.prices, methods=['GET'])
        self.add_url_rule('/rates', 'rates', backend.rates, methods=['GET'])
        # self.register_blueprint(swagger_ui_blueprint)
