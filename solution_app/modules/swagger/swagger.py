from flask import Blueprint
from flask_swagger_ui import get_swaggerui_blueprint

from solution_app.services.settings import SETTINGS


def read_swagger_config():
    with open('solution_app/static/swagger.yaml') as f:
        ret = f.read()
    return ret


class SwaggerBlueprint(Blueprint):
    SWAGGER_URL = '/swagger'
    API_URL = f'http://{SETTINGS["FLASK_RUN_HOST"]}:{SETTINGS["FLASK_RUN_PORT"]}/docs/v2/swagger.yaml'

    def __init__(self):
        super().__init__('swagger', __name__)
        self.add_url_rule('/docs/v2/swagger.yaml', 'swagger_docs', read_swagger_config, methods=['GET'])

        swagger_ui_blueprint = get_swaggerui_blueprint(
            self.SWAGGER_URL,
            self.API_URL)
        self.register_blueprint(swagger_ui_blueprint)



