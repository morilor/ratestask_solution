"""
SETTINGS is config.env file variables with actual env variables as overrides.
"""

import os
from dotenv import dotenv_values

default_settings = dotenv_values('config.env')

SETTINGS = {
    key: os.getenv(key, val)
    for key, val in default_settings.items()
}
