from abc import ABC, abstractmethod

from flask import request, jsonify, Response
import traceback
import logging
from solution_app.models.model import ModelErrorDB, ModelErrorInput, ModelAPI
from solution_app.services.settings import SETTINGS
from functools import wraps

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(SETTINGS['LOGGING_LEVEL'])


class BackendAPI(ABC):
    """
    BackendAPI handles communication between the app and the data layer, prepares raw data drom the model before
    serving it to the app.
    """
    def __init__(self, model: ModelAPI):
        self.model = model

    @abstractmethod
    def status(self) -> (Response, int):
        pass

    @abstractmethod
    def subregions(self) -> (Response, int):
        pass

    @abstractmethod
    def ports(self) -> (Response, int):
        pass

    @abstractmethod
    def prices(self) -> (Response, int):
        pass

    @abstractmethod
    def rates(self) -> (Response, int):
        pass


class Backend(BackendAPI):
    @staticmethod
    def _model_error_handler(func):
        # noinspection PyBroadException
        @wraps(func)
        def new_func(*args, **kwargs):
            try:
                result = func(*args, **kwargs)
            except ModelErrorDB:
                logger.error(traceback.format_exc())
                return jsonify('Data could not be accessed.'), 404
            except ModelErrorInput:
                logger.error(traceback.format_exc())
                return jsonify('Invalid query parameters.'), 400
            # we protect the solution_app from showing inner exceptions and, therefore, code details.
            except BaseException:
                logger.error(traceback.format_exc())
                return jsonify('Internal server error.'), 500
            else:
                return result

        return new_func

    def status(self) -> (Response, int):
        return jsonify(self.model.get_status()), 200

    @_model_error_handler
    def subregions(self) -> (Response, int):
        logger.info(f'get_subregions <- {request.full_path}')
        result = self.model.get_subregions(request.args)
        logger.info(f'get_subregions -> {len(result)} entries')
        return jsonify([row[0] for row in result]), 200  # flatten for readability

    @_model_error_handler
    def ports(self) -> (Response, int):
        logger.info(f'get_ports <- {request.full_path}')
        result = self.model.get_ports(request.args)
        logger.info(f'get_ports -> {len(result)} entries')
        return jsonify([row[0] for row in result]), 200  # flatten for readability

    @_model_error_handler
    def prices(self) -> (Response, int):
        logger.info(f'get_prices <- {request.full_path}')
        result = self.model.get_prices(request.args)
        logger.info(f'get_prices -> {len(result)} entries')
        pretty_results = [
            {
                "origin": row[0],
                "destination": row[1],
                "day": row[2].strftime("%Y-%m-%d"),
                "price": None if row[3] is None else int(row[3]),
            }
            for row in result
        ]
        return jsonify(pretty_results), 200

    @_model_error_handler
    def rates(self) -> (Response, int):
        logger.info(f'get_rates <- {request.full_path}')
        result = self.model.get_rates(request.args)
        logger.info(f'get_rates -> {len(result)} entries')
        pretty_results = [
            {
                "day": row[0].strftime("%Y-%m-%d"),
                "average_price": None if row[1] is None else int(row[1]),
            }
            for row in result
        ]
        return jsonify(pretty_results), 200
