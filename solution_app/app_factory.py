"""
The app factory manages the building of the whole application. If a module is added, it should be connected here.
"""

from flask import Flask
from solution_app.modules.solution.routes import SolutionBlueprint
from solution_app.modules.swagger.swagger import SwaggerBlueprint
from solution_app.services.backend import BackendAPI


def build_app(app: Flask, backend: BackendAPI):
    solution_bl = SolutionBlueprint(backend)
    swagger_bl = SwaggerBlueprint()
    app.register_blueprint(solution_bl, url_prefix="/")
    app.register_blueprint(swagger_bl)
    return app
