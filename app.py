from flask import Flask

from solution_app.app_factory import build_app
from solution_app.models.model import ModelSQL
from solution_app.services.backend import Backend

model = ModelSQL()
backend = Backend(model)
# flask and gunicorn entrypoint
app = build_app(Flask('ratestask_solution'), backend)
